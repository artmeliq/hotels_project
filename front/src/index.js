import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import {applyMiddleware, compose, createStore} from "redux";
import reducer from "./store/reducer/index";
import createSagaMiddleware from 'redux-saga';
import watcher from "./store/saga/index";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const saga=createSagaMiddleware()

const store=createStore(
    reducer,
    composeEnhancers(
        applyMiddleware(saga)
    )
)
saga.run(watcher)

window.store=store

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
         <App />
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
