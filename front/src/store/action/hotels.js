export const GET_DATA_HOTELS_SUCSSES='GET_DATA_HOTELS_SUCSSES';
export const GET_DATA_HOTELS_FAILD='GET_DATA_HOTELS_FAILD';
export const GET_DATA_HOTELS='GET_DATA_HOTELS';
export function getDataHotels(page,region){
    return{
        type:GET_DATA_HOTELS,
        payload:{
            page,
            region
        }
    }
}


export const GET_HOTELS_USER_SUCSSES='GET_HOTELS_USER_SUCSSES';
export const GET_HOTELS_USER_FAILD='GET_HOTELS_USER_FAILD';
export const GET_HOTELS_USER='GET_HOTELS_USER';
export function getHotelUser(){
    return{
        type:GET_HOTELS_USER,
    }
}


export const GET_DELETE_HOTEL='GET_DELETE_HOTEL';
export const GET_DELETE_HOTEL_SUCCESS='GET_DELETE_HOTEL_SUCCESS';
export const GET_DELETE_HOTEL_FILE='GET_DELETE_HOTEL_FILE';
export function getDeleteHotel(id){
    return{
        type:GET_DELETE_HOTEL,
        payload:{
            id
        }
    }
}



export const GET_UPDATE_HOTELS_SUCSSES='GET_UPDATE_HOTELS_SUCSSES';
export const GET_UPDATE_HOTELS_FAILD='GET_UPDATE_HOTELS_FAILD';
export const GET_UPDATE_HOTELS='GET_UPDATE_HOTELS';
export function getUpdateHotel(hotelData){
    return{
        type:GET_UPDATE_HOTELS,
        payload:{
            hotelData
        }
    }
}


export const GET_FOOL_DATA_HOTELS_SUCSSES='GET_FOOL_DATA_HOTELS_SUCSSES';
export const GET_FOOL_DATA_HOTELS_FAILD='GET_FOOL_DATA_HOTELS_FAILD';
export const GET_FOOL_DATA_HOTELS='GET_FOOL_DATA_HOTELS';
export function getFoolDataHotel(id){
    return{
        type:GET_FOOL_DATA_HOTELS,
        payload:{
            id
        }
    }
}

export const GET_MAP_SUCSSES='GET_MAP_SUCSSES';
export const GET_MAP_FAILD='GET_MAP_FAILD';
export const GET_MAP='GET_MAP';
export function getArmeniaMap(){
    return{
        type:GET_MAP,
    }
}