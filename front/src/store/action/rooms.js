
export const GET_ROOMS_CREATE_SUCCESS='GET_ROOMS_CREATE_SUCCESS';
export const GET_ROOMS_CREATE_FAIL='GET_ROOMS_CREATE_FAIL';
export const GET_ROOMS_CREATE='GET_ROOMS_CREATE';

export function getRoomsCreate(file,status,count,price,hotelId){
  return{
    type:GET_ROOMS_CREATE,
    payload:{
      file,
      status,
      count,
      price,
      hotelId
    }
  }
}
export const GET_ROOMS_BOOKING_SUCCESS='GET_ROOMS_BOOKING_SUCCESS'
export const GET_ROOMS_BOOKING_FAIL='GET_ROOMS_BOOKING_FAIL'
export const GET_ROOMS_BOOKING='GET_ROOMS_BOOKING'

export function getRoomsBooking(booking){
  return{
    type:GET_ROOMS_BOOKING,
    payload:{
      booking
    }
  }
}
export const GET_YOUR_BOOKING_SUCCESS='GET_YOUR_BOOKING_SUCCESS'
export const GET_YOUR_BOOKING_FAIL='GET_YOUR_BOOKING_FAIL'
export const GET_YOUR_BOOKING='GET_YOUR_BOOKING'

export function getYourBooking(){
  return{
    type:GET_YOUR_BOOKING,
  }
}