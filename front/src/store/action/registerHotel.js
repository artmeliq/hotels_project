
export const GET_REGISTER_HOTEL_SUCCESS='GET_REGISTER_HOTEL_SUCCESS';
export const GET_REGISTER_HOTEL_FAILED='GET_REGISTER_HOTEL_FAILED';
export const GET_REGISTER_HOTEL='GET_REGISTER_HOTEL';

export function getRegisterHotel(hotelData){
    return{
        type:GET_REGISTER_HOTEL,
        payload:{
            hotelData
        }
    }
}
export const GET_PHOTO_SUCCESS='GET_PHOTO_SUCCESS';
export const GET_PHOTO_FAILED='GET_PHOTO_FAILED';
export const GET_PHOTO_HOTEL='GET_PHOTO_HOTEL';
export function getPhotoUpload(id,file){
    return{
        type:GET_PHOTO_HOTEL,
        payload:{
            id,file
        }
    }
}
