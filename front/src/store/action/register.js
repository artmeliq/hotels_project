
export const GET_REGISTER_USERS_SUCSSES='GET_REGISTER_USERS_SUCSSES';
export const GET_REGISTER_USERS_FAILD='GET_REGISTER_USERS_FAILD';
export const GET_REGISTER_USERS='GET_REGISTER_USERS';

export function getRegister(DataUser){
    return{
        type:GET_REGISTER_USERS,
        payload:{
            DataUser
        }
    }
}

export const GOOGLE_REG_REQUEST = 'GOOGLE_REG_REQUEST';
export const GOOGLE_REG_SUCCESS = 'GOOGLE_REG_SUCCESS';
export const GOOGLE_REG_FAIL = 'GOOGLE_REG_FAIL';

export function googleCookie(token) {
    return {
        type: GOOGLE_REG_REQUEST,
        payload: {
            token,
        }
    }
}
