
export const GET_LOGIN_USERS_SUCSSES='GET_LOGIN_USERS_SUCSSES';
export const GET_LOGIN_USERS_FAILD='GET_LOGIN_USERS_FAILD';
export const GET_LOGIN_USERS='GET_LOGIN_USERS';

export function getLoginUser(email,password,cb){
    return{
        type:GET_LOGIN_USERS,
        payload:{
            email,
            password,
            cb
        }
    }
}

export const GET_DATA_USERS_SUCSSES='GET_DATA_USERS_SUCSSES';
export const GET_DATA_USERS_FAILD='GET_DATA_USERS_FAILD';
export const GET_DATA_USERS='GET_DATA_USERS';

export function getDataUser(){
    return{
        type:GET_DATA_USERS,
        payload:{
        }
    }
}
export const GET_UPDATE_PASSWORD_SUCSSES='GET_UPDATE_PASSWORD_SUCSSES';
export const GET_UPDATE_PASSWORD_FAILD='GET_UPDATE_PASSWORD_FAILD';
export const GET_UPDATE_PASSWORD='GET_UPDATE_PASSWORD';

export function getUpdatePassword(password){
    return{
        type:GET_UPDATE_PASSWORD,
        payload:{
           password
        }
    }
}
