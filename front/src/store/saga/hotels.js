import {call, put, takeLatest} from "redux-saga/effects";
import Api from "../../helpers/Api";
import {
    GET_DATA_HOTELS,
    GET_DATA_HOTELS_FAILD,
    GET_DATA_HOTELS_SUCSSES,
    GET_DELETE_HOTEL,
    GET_DELETE_HOTEL_FILE,
    GET_DELETE_HOTEL_SUCCESS,
    GET_FOOL_DATA_HOTELS,
    GET_FOOL_DATA_HOTELS_FAILD,
    GET_FOOL_DATA_HOTELS_SUCSSES,
    GET_HOTELS_USER,
    GET_HOTELS_USER_FAILD,
    GET_HOTELS_USER_SUCSSES,
    GET_MAP,
    GET_MAP_FAILD,
    GET_MAP_SUCSSES,
    GET_UPDATE_HOTELS,
    GET_UPDATE_HOTELS_FAILD,
    GET_UPDATE_HOTELS_SUCSSES,
} from "../action/hotels";

export default function* watcher(){
    yield takeLatest(GET_DATA_HOTELS,handleDataHotels)
    yield takeLatest(GET_FOOL_DATA_HOTELS,handleFoolHotelData)
    yield takeLatest(GET_MAP,handleMap)
    yield takeLatest(GET_HOTELS_USER,hotelUser)
    yield takeLatest(GET_DELETE_HOTEL,deleteHotel)
    yield takeLatest(GET_UPDATE_HOTELS,handleUpdateHotel)
}
function* handleDataHotels(action) {
    try {
        const {page} = action.payload
        const {region} = action.payload
        const {data} = yield call(Api.getDataHotelAll, page,region)
        yield put({
            type: GET_DATA_HOTELS_SUCSSES,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_DATA_HOTELS_FAILD,
            massage: e.massage
        })
    }

}
function* handleFoolHotelData(action) {
    try {

        const {id} = action.payload
        const {data} = yield call(Api.getHotelDataFool, id)
        yield put({
            type: GET_FOOL_DATA_HOTELS_SUCSSES,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_FOOL_DATA_HOTELS_FAILD,
            massage: e.massage
        })
    }

}
function* handleUpdateHotel(action) {
    try {
        const {hotelData} = action.payload
        const {data} = yield call(Api.getHotelUpdate, hotelData)
        yield put({
            type: GET_UPDATE_HOTELS_SUCSSES,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_UPDATE_HOTELS_FAILD,
            massage: e.massage
        })
    }

}
function* handleMap() {
    try {
        const {data} = yield call(Api.mapData)
        yield put({
            type: GET_MAP_SUCSSES,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_MAP_FAILD,
            massage: e.massage
        })
    }

}
function* hotelUser() {
    try {
        const {data} = yield call(Api.hotelUser)
        yield put({
            type: GET_HOTELS_USER_SUCSSES,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_HOTELS_USER_FAILD,
            massage: e.massage
        })
    }

}
function* deleteHotel(action) {
    try {
        const {id}=action.payload
        const {data} = yield call(Api.deleteHotel,id)
        yield put({
            type: GET_DELETE_HOTEL_SUCCESS,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_DELETE_HOTEL_FILE,
            massage: e.massage
        })
    }

}