import {call, put, takeLatest} from "redux-saga/effects";
import {
  GET_ROOMS_BOOKING, GET_ROOMS_BOOKING_FAIL,
  GET_ROOMS_BOOKING_SUCCESS,
  GET_ROOMS_CREATE,
  GET_ROOMS_CREATE_FAIL,
  GET_ROOMS_CREATE_SUCCESS, GET_YOUR_BOOKING, GET_YOUR_BOOKING_FAIL, GET_YOUR_BOOKING_SUCCESS
} from "../action/rooms";
import Api from "../../helpers/Api";


export default function* watcher(){
  yield takeLatest(GET_ROOMS_CREATE,handleRoomsCreate)
  yield takeLatest(GET_ROOMS_BOOKING,handleRoomsBooking)
  yield takeLatest(GET_YOUR_BOOKING,handleYourBooking)
}

function* handleRoomsCreate(action){
  try {
      const {file}=action.payload
      const {status}=action.payload
      const {count}=action.payload
      const {price}=action.payload
      const {hotelId}=action.payload
      const {data}=yield call(Api.getRoomsCreate,file,status,count,price,hotelId)

    yield  put({
      type:GET_ROOMS_CREATE_SUCCESS,
      payload:{
        data:data
      }
    })
  }
  catch (e){
    yield put({
      type:GET_ROOMS_CREATE_FAIL,
      massage:e.massage
    })
  }
}
function* handleRoomsBooking(action){
  try {

      const {booking}=action.payload
      const {data}=yield call(Api.getRoomBooking,booking)
    yield  put({
      type:GET_ROOMS_BOOKING_SUCCESS,
      payload:{
        data:data
      }
    })
  }
  catch (e){
    yield put({
      type:GET_ROOMS_BOOKING_FAIL,
      massage:e.massage
    })
  }
}
function* handleYourBooking(){
  try {
      const {data}=yield call(Api.getYourBooking)
    yield  put({
      type:GET_YOUR_BOOKING_SUCCESS,
      payload:{
        data:data
      }
    })
  }
  catch (e){
    yield put({
      type:GET_YOUR_BOOKING_FAIL,
      massage:e.massage
    })
  }
}