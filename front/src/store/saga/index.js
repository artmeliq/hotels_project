import { all, fork } from 'redux-saga/effects';
import login from './login';
import rooms from './rooms';
import register from './register';
import hotels from './hotels';
import registerHotel from './registerHotel';

export default function* watchers() {
    yield all([
        login,
        register,
        registerHotel,
        hotels,
         rooms
    ].map(fork));
}
