import {call, put, takeLatest} from "redux-saga/effects";
import {
    GET_LOGIN_USERS,
    GET_LOGIN_USERS_FAILD,
    GET_LOGIN_USERS_SUCSSES,
    GET_DATA_USERS,
    GET_DATA_USERS_FAILD,
    GET_DATA_USERS_SUCSSES,
    GET_UPDATE_PASSWORD_SUCSSES,
    GET_UPDATE_PASSWORD_FAILD,
    GET_UPDATE_PASSWORD
} from "../action/login";
import Api from "../../helpers/Api";


export default function* watcher(){
    yield takeLatest(GET_LOGIN_USERS,handleLoginData)
    yield takeLatest(GET_DATA_USERS,handleDataUser)
    yield takeLatest(GET_UPDATE_PASSWORD,handleUpdatePassword)
}

function* handleLoginData(action){
    try {
        const {email}=action.payload
        const {password}=action.payload
        const {data}=yield call(Api.getLoginUser,email,password)

        yield  put({
            type:GET_LOGIN_USERS_SUCSSES,
            payload:{
                data:data
            }
        })
        if (action.payload.cb){
            action.payload.cb(null,data)
        }
    }
    catch (e){
        yield put({
            type:GET_LOGIN_USERS_FAILD,
           massage:e.massage
        })
        if (action.payload.cb) {
            action.payload.cb(e, e.response?.data || {})
        }
    }
}
function* handleDataUser(action){
    try {
        const {data}=yield call(Api.getUserData)
        yield  put({
            type:GET_DATA_USERS_SUCSSES,
            payload:{
                data:data
            }
        })
    }
    catch (e){
        yield put({
            type:GET_DATA_USERS_FAILD,
            massage:e.massage
        })
    }
}
function* handleUpdatePassword(action){
    try {
        const {password}=action.payload
        const {data}=yield call(Api.getUpdatePassword,password)
        yield  put({
            type:GET_UPDATE_PASSWORD_SUCSSES,
            payload:{
                data:data
            }
        })
    }
    catch (e){
        yield put({
            type:GET_UPDATE_PASSWORD_FAILD,
            massage:e.massage
        })
    }
}