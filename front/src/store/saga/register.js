import {call, put, takeLatest} from "redux-saga/effects";
import {
    GET_REGISTER_USERS,
    GET_REGISTER_USERS_FAILD,
    GET_REGISTER_USERS_SUCSSES, GOOGLE_REG_FAIL,
    GOOGLE_REG_REQUEST, GOOGLE_REG_SUCCESS
} from "../action/register";
import Api from "../../helpers/Api";


export default function* watcher(){
    yield takeLatest(GET_REGISTER_USERS,handleRegisterData)
    yield takeLatest(GOOGLE_REG_REQUEST,handleGoogle)
}

function* handleRegisterData(action){

    try {
        const {DataUser}=action.payload
        const {data}=yield call(Api.getRegisterUser,DataUser)
        yield  put({
            type:GET_REGISTER_USERS_SUCSSES,
            payload:{
                data:data
            }
        })
    }
    catch (e){
        yield put({
            type:GET_REGISTER_USERS_FAILD,
            massage:e.massage
        })
    }
}

function* handleGoogle(action){
    try {
        const {token} =action.payload;
        const {data}  = yield call(Api.google,token);
        yield put({
            type:GOOGLE_REG_SUCCESS,
            payload:{
                data
            }
        })
    }catch (e) {
        console.warn(e);
        yield put({
            type:GOOGLE_REG_FAIL,
            message:e.message
        })
    }
}
