import {call, put, takeLatest} from 'redux-saga/effects';
import {
    GET_REGISTER_HOTEL_SUCCESS,
    GET_REGISTER_HOTEL_FAILED,
    GET_REGISTER_HOTEL,
    GET_PHOTO_HOTEL,
    GET_PHOTO_SUCCESS,
    GET_PHOTO_FAILED
} from '../action/registerHotel';
import Api from '../../helpers/Api'

export default function* watcher(){
    yield takeLatest(GET_REGISTER_HOTEL, handleRegisterHotel)
    yield takeLatest(GET_PHOTO_HOTEL, handlePhotoUpload)
}

function* handleRegisterHotel(action) {
    try {
        const {hotelData} = action.payload
        const {data} = yield call(Api.getRegisterHotel, hotelData)

        yield put({
            type: GET_REGISTER_HOTEL_SUCCESS,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_REGISTER_HOTEL_FAILED,
            massage: e.massage
        })
    }

}
function* handlePhotoUpload(action) {
    try {
        const {id} = action.payload
        const {file} = action.payload
        const {data} = yield call(Api.getFileUpload, id,file)
        yield put({
            type: GET_PHOTO_SUCCESS,
            payload: {
                data: data
            }
        })
    }catch (e){
        yield put({
            type: GET_PHOTO_FAILED,
            massage: e.massage
        })
    }

}
