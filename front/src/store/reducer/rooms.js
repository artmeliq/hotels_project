import {
    GET_ROOMS_BOOKING,
    GET_ROOMS_BOOKING_FAIL,
    GET_ROOMS_BOOKING_SUCCESS,
    GET_ROOMS_CREATE, GET_ROOMS_CREATE_FAIL,
    GET_ROOMS_CREATE_SUCCESS, GET_YOUR_BOOKING, GET_YOUR_BOOKING_FAIL, GET_YOUR_BOOKING_SUCCESS
} from "../action/rooms";
import {error, success} from "../../components/svg/Popup";

const initialState= {
   bookingStatus:'',
    YourBooking:[]

}


export default function reducer(state=initialState,action){
    switch (action.type) {
        case GET_ROOMS_BOOKING:{
            return{
                ...state,

            }
        }
        case GET_ROOMS_BOOKING_SUCCESS:{
            success("The reservation was made normally")
            return{
                ...state,
                bookingStatus: true
            }
        }
        case GET_ROOMS_BOOKING_FAIL:{
            error("Booking dates are busy")
            return{
                ...state,
                bookingStatus: false

            }
        }
        case GET_YOUR_BOOKING:{
            return{
                ...state,

            }
        }
        case GET_YOUR_BOOKING_SUCCESS:{
            return{
                ...state,
                YourBooking:action.payload.data.booking,
            }
        }
        case GET_YOUR_BOOKING_FAIL:{
            return{
                ...state,
            }
        }
        case GET_ROOMS_CREATE:{
            return{
                ...state,
            }
        }
        case GET_ROOMS_CREATE_SUCCESS:{
            success("The old room has been created")
            return{
                ...state,
                bookingStatus: true
            }
        }
        case GET_ROOMS_CREATE_FAIL:{
            error("Please correct the mistakes")
            return{
                ...state,
                bookingStatus: false

            }
        }
        default:{
            return {
                ...state,
                bookingStatus: "error"
            }
        }

    }
}
