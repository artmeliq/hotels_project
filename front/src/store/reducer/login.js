import {GET_LOGIN_USERS,
        GET_LOGIN_USERS_FAILD,
        GET_LOGIN_USERS_SUCSSES,
        GET_DATA_USERS,
        GET_DATA_USERS_FAILD,
        GET_DATA_USERS_SUCSSES} from "../action/login";
import Cookies from "js-cookie"
import {UserHello} from "../../components/svg/Popup";


const initialState= {
    loginMessage : "",
    UserDataMessage:'',
    loginToken :Cookies.get("token"),
    dataUser:{},
    Data_User:{}

}


export default function reducer(state=initialState,action){
    switch (action.type) {
        case GET_LOGIN_USERS:{
            return{
                ...state,
                loginMessage: "Request"

            }
        }
        case GET_LOGIN_USERS_SUCSSES:{
            const loginToken= action.payload.data.token || ""
            return{
                ...state,
                loginToken,
                loginMessage: action.payload.data.error,
                dataUser: action.payload.data.user

            }
        }
        case GET_LOGIN_USERS_FAILD:{
            return{
                ...state,
                loginMessage: "Fail",
                message:action.message

            }
        }
        case GET_DATA_USERS:{
            return{
                ...state,
                UserDataMessage:"Request"

            }
        }
        case GET_DATA_USERS_SUCSSES:{
            UserHello(`Hello ${action.payload.data.data.name}`)
            console.log(action)
            return{
                ...state,
                UserDataMessage: action.payload.data.error,
                Data_User: action.payload.data.data

            }
        }
        case GET_DATA_USERS_FAILD:{
            console.log(action)
            return{
                ...state,
                UserDataMessage: "Fail",
                message:action.message

            }
        }
        default:{
            return {
                ...state,
                loginMessage: "error"
            }
        }

    }
}
