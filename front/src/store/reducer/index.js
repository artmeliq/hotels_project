import login from './login'
import register from './register'
import registerHotel from './registerHotel'
import hotels from "./hotels"
import rooms from "./rooms"
import { combineReducers } from "redux";

export default combineReducers({
    login,
    register,
    registerHotel,
    hotels,
    rooms
})