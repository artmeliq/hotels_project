import {
    GET_REGISTER_USERS,
    GET_REGISTER_USERS_FAILD,
    GET_REGISTER_USERS_SUCSSES, GOOGLE_REG_FAIL, GOOGLE_REG_REQUEST,
    GOOGLE_REG_SUCCESS
} from "../action/register";


const initialState= {
  status:'',
    token:'',
    data:{}
}


export default function reducer(state=initialState,{type,payload}){
    switch (type) {
        case GET_REGISTER_USERS:{
            return{
                ...state,
                loginMessage: "Request"

            }
        }
        case GET_REGISTER_USERS_SUCSSES:{
            return{
                ...state,
                status: payload.data.status,


            }
        }
        case GET_REGISTER_USERS_FAILD:{
            return{
                ...state,
                loginMessage: "Fail",

            }
        }
        case GOOGLE_REG_REQUEST:{
            return {
                ...state,
                status: 'request'
            }
        }
        case GOOGLE_REG_SUCCESS:{
            return {
                ...state,
                data:payload.data,
                token: payload.data?.token,
                status: 'success'
            }
        }
        case GOOGLE_REG_FAIL:{
            return {
                ...state,
                status: 'fail',
            }
        }
        default:{
            return {
                ...state,
                loginMessage: "error"
            }
        }

    }
}
