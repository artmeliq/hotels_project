import {GET_REGISTER_HOTEL_SUCCESS,
        GET_REGISTER_HOTEL_FAILED,
        GET_REGISTER_HOTEL} from "../action/registerHotel";
import {error} from "../../components/svg/Popup";

const initialState = {
    status: '',
    data_hotel:{}
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_REGISTER_HOTEL: {
            return {
                ...state,
                loginMessage: "Request"
            }
        }
        case GET_REGISTER_HOTEL_SUCCESS: {
            return {
                ...state,
                status: true,
                data_hotel: action.payload.data.data,
            }
        }
        case GET_REGISTER_HOTEL_FAILED: {
            error("Hotel Register filed")
            return {
                ...state,
                loginMessage: "Faild",
                status:false,
                message: action.message
            }
        }

        default: {
            return {
                ...state,
                loginMessage: "error"
            }
        }
    }
}
