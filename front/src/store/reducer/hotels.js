import {
    GET_DATA_HOTELS,
    GET_DATA_HOTELS_FAILD,
    GET_DATA_HOTELS_SUCSSES, GET_DELETE_HOTEL, GET_DELETE_HOTEL_FILE, GET_DELETE_HOTEL_SUCCESS,
    GET_FOOL_DATA_HOTELS,
    GET_FOOL_DATA_HOTELS_FAILD,
    GET_FOOL_DATA_HOTELS_SUCSSES,
    GET_HOTELS_USER,
    GET_HOTELS_USER_FAILD,
    GET_HOTELS_USER_SUCSSES,
    GET_MAP,
    GET_MAP_FAILD,
    GET_MAP_SUCSSES
} from "../action/hotels";
import {error, success} from "../../components/svg/Popup";

const initialState = {
    hotelData:[],
    status: '',
    HotelsMessage: '',
    photoHotel:{},
    Data_Hotels: [],
    hotel_user:[],
    pageCount:1,
    map_Armenia:[],
    region:null
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case GET_DATA_HOTELS: {
            return {
                ...state,
                HotelsMessage: "Request"
            }
        }
        case GET_DATA_HOTELS_SUCSSES: {
            return {
                ...state,
                status: action.payload.data.status,
                Data_Hotels: action.payload.data.hotels,
                pageCount: action.payload.data.pageCount,
                region: action.payload.data.region,
            }
        }
        case GET_DATA_HOTELS_FAILD: {
            return {
                ...state,
                HotelsMessage: "Faild",
                message: action.message
            }
        }
        case GET_FOOL_DATA_HOTELS: {
            return {
                ...state,
                HotelsMessage: "Request"
            }
        }
        case GET_FOOL_DATA_HOTELS_SUCSSES: {
           let hotels=action.payload.data.hotels
            hotels[0].photo=action.payload.data.photo
            return {
                ...state,
                status: action.payload.data.status,
                hotelData: [...hotels],

            }
        }
        case GET_FOOL_DATA_HOTELS_FAILD: {
            return {
                ...state,
                HotelsMessage: "Faild",
                message: action.message
            }
        }
        case GET_MAP: {
            return {
                ...state,
                HotelsMessage: "Request"
            }
        }
        case GET_MAP_SUCSSES: {
            return {
                ...state,
                status: action.payload.data.status,
                map_Armenia: action.payload.data.data,
            }
        }
        case GET_MAP_FAILD: {
            return {
                ...state,
                HotelsMessage: "Faild",
                message: action.message
            }
        }
        case GET_HOTELS_USER: {
            return {
                ...state,
                HotelsMessage: "Request"
            }
        }
        case GET_HOTELS_USER_SUCSSES: {
            return {
                ...state,
                hotel_user: action.payload.data.hotels,
            }
        }
        case GET_HOTELS_USER_FAILD: {
            return {
                ...state,
                HotelsMessage: "Faild",
                message: action.message
            }
        }  case GET_DELETE_HOTEL: {
            return {
                ...state,
                HotelsMessage: "Request"
            }
        }
        case GET_DELETE_HOTEL_SUCCESS: {
            success("Hotels deleted")
            return {
                ...state,
            }
        }
        case GET_DELETE_HOTEL_FILE: {
            error("Wrong to delete hotel")
            return {
                ...state,
                HotelsMessage: "Faild",
            }
        }

        default: {
            return {
                ...state,
                HotelsMessage: "error"
            }
        }
    }
}
