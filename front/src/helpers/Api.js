import axios from "axios";
import {serialize} from "object-to-formdata";


const api = axios.create({
  baseURL: 'http://localhost:4000/',
})

api.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  if (token) {
    config.headers.Authorization = token
  }
  return config

}, (err) => Promise.reject(err))


class Api {
  static getLoginUser = (email, password) => {
    return (
      api.post('users/login', {
          email: email,
          password: password,
        },
      )
    )
  }
  static getRegisterUser = (data) => {

    return (
      api.post('users/register', {
          email: data.email.email,
          password: data.Repeat_password.Repeat_password,
          name: data.Name.Name,
          surName: data.Surname.Surname,
          phone: data.phone.phone,
          gender: data.Gender.Gender,
          role:data.Role.Role
        }
      )
    )
  }
  static hotelUser = () => {
    return (
      api.get('hotels/user/hotel')
    )
  }
  static  getUserData = () => {
    return (
      api.get('users/get/data')
    )
  }
  static  getDataHotelAll = (page,region) => {
    return (
      api.get(`hotels/getHotels`,{
          params:{
              page:page,
              region:region
          }
      })
    )
  }
  static  getHotelDataFool = (id) => {
    return (
      api.get(`hotels/getFoolHotel`,{
          params:{
             id:id
          }
      })
    )
  }
  static  deleteHotel = (id) => {
    return (
      api.delete(`hotels/delete/${id}`)
    )
  }
  static google = (token)=>{
      return api.post('/users/google?accessToken=' + token)
  }
    static getRegisterHotel = (hotelData) => {
        return (
            api.post('hotels/create', {
              ...hotelData,
                location:'aaa'
            })
        )
    }
    static getHotelUpdate = (hotelData) => {
        return (
            api.put('hotels/update', {
                ...hotelData,
                location:'aAA'
            })
        )
    }
    static getUpdatePassword = (password) => {
        return (
            api.put('users/update/password', {
               ...password
            })
        )
    }
    static getRoomBooking = (booking) => {
        return (
            api.post('booking/create', {
               ...booking
            })
        )
    }
    static getFileUpload=(hotelId,file)=>{
        const formData = serialize({
                hotelId,
                file
        }
        );
      return(
        api.post("photos/upload",formData)
      )
    }
    static mapData = ()=>{
        return api.get('/map/get')
    }
    static getYourBooking = ()=>{
        return api.get('/booking/user-data')
    }
    static getRoomsCreate = (file,status,count,price,hotelId)=>{

    const formData = serialize({
      'files[]':file,
      status:status,
      roomCount:count,
      price:price+'',
      hotelId:hotelId
    })
      return api.post('/rooms/create', formData)
    }
}


export default Api
