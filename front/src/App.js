import './components/assetes/css/style.css'
import './components/assetes/css/_style.css'
import './components/assetes/css/_slide.css'
import './components/assetes/css/popup.scss'
import './components/assetes/css/_regHotel.css'
import './components/assetes/css/login.css'
import 'react-calendar/dist/Calendar.css';
import 'react-toastify/dist/ReactToastify.css';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About";
import Register from "./components/Register";
import Login from "./pages/Login";
import RegisterHotel from "./components/register/RegisterHotel";
import Rooms from "./pages/Rooms";
import router from "./data/router.json";
import _ from "lodash";
import {ToastContainer} from "react-toastify";
const Page = {
  'Home': <Home/>,
  "About":<About/>,
  "Register":<Register/>,
  "Login":<Login/>,
  "RegisterHotel":<RegisterHotel/>,
  "Rooms":<Rooms/>
}


function App() {
    return (
      <>
        <BrowserRouter>
                <Routes>
                    {_.map(router,(router=>{
                        return <Route key={router.path} path={router.path} element={Page[router.element]}/>
                    }))}
                    <Route path="*" element={<Navigate replace to="/"/>}/>

                </Routes>
        </BrowserRouter>
          <ToastContainer
              position="top-center"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
          />
  </>
    );
}
export default App;
