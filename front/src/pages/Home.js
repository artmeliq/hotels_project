import React, {useEffect} from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import Main from "./Main";
import {StyledContainer} from "../components/styledComponents/StyledComponentsAbout";
import {error} from "../components/svg/Popup";
import TableBooking from "../components/Booking/TableBooking";



function Home(props) {
    return (
        <StyledContainer>
            <TableBooking/>
            <Header />
                <Main/>
            <Footer/>
        </StyledContainer>
    );
}

export default Home;