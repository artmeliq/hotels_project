import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import _ from "lodash"
import {getFoolDataHotel} from "../store/action/hotels";
import {useLocation, useNavigate, useParams} from "react-router-dom";
import Slide from "../components/Slide/Slide";
import {
    StyledBookingButton,
    StyledBookingContainer,
    StyledBookingInput, StyledBookingInputCheck,
    StyledBookingSpan
} from "../components/styledComponents/StyledBookin";
import {StyledBlockBooking, StyledBlockDiv, StyledTitleH3} from "../components/styledComponents/StyledMain";
import {getRoomsBooking} from "../store/action/rooms";

const menu = [{
    data_group: 'all',
    name: 'all',
    count: 'all',
    hover: true
}, {
    data_group: 'webdesign',
    name: '2 lines',
    count: 2
}, {
    data_group: 'webdev',
    name: 'family',
    count: 3
}, {
    data_group: 'mobileapps',
    name: 'separately',
    count: 1
}]

function Rooms(props) {
    const navigator=useNavigate()
    const dispatch = useDispatch()
    const loc = useParams()
    const hotel = useSelector(store => store.hotels.hotelData)
    const [rooms, setRooms] = useState([])
    const [photo, setPhoto] = useState([])
    const [idRoom, setIdRoom] = useState(null)
    const [showBooking, setShowBooking] = useState(false)
    const [slide, setSlide] = useState(false)
    if (hotel.length === 0) {
        dispatch(getFoolDataHotel(loc.hotelID))
    }
    useEffect(() => {
        handleFilter('all')
    }, [])

    const handleFilter = (count) => {
        console.log(count)
        if (hotel.length === 0) {
            return
        }
        _.map(menu, (item) => {
            if (item.count === count) {
                item.hover = true
            } else {
                item.hover = false
            }
        })
        if (count === 'all') {
            setRooms(hotel[0].room || [])
            return;
        }
        let _room = hotel[0].room.filter(room => room.roomCount === +count)
        setRooms(_room)
    }
    const handleBooking = (id) => {
        setIdRoom(id)
        setShowBooking(true)
    }
    const handleSlide = (room) => {
        setPhoto(room.photo)
        setSlide(true)
    }
    return (
        <>
            <section className="site-section section-works" id="works">
                {slide ? <Slide photo={photo}/> : null}
                <div className="container">
                    <h2 style={{cursor:'pointer'}} onClick={()=>{navigator('/')}}>Hotels</h2>
                    <p className="section-subtitle"><span>OUR CLIENTS LOVE US! READ WHAT THEY HAVE TO SAY</span></p>
                    <div className="portfolio">
                        <ul className="portfolio-sorting list-inline">
                            {_.map(menu, (item) => {
                                return (
                                    <li><a href="#" className={item.hover ? " active" : ''} data-group={item.data_group}
                                           onClick={(ev) => {
                                               ev.preventDefault()
                                               handleFilter(item.count, ev)
                                           }}>
                                        {item.name}</a></li>)
                            })}
                        </ul>
                        <div id="grid">
                            {_.map(rooms, (rooms => {
                                console.log(rooms)
                                return (<div className="col-md-3 col-sm-4 col-xs-6" data-groups='["webdesign"]'
                                             style={{margin: 10, width: '20%'}} onClick={() => {
                                    handleSlide(rooms)
                                }}>
                                    <div className="portfolio-item">
                                        <div className="portfolio-item-thumb">
                                            <img
                                                src={'http://localhost:4000' + rooms.photo[0].url.replaceAll("\\", "\\\\")}
                                                alt="" className="img-res"/>
                                            <a href="#" className="rectangle" data-toggle="modal"
                                               data-target="#portfolioItem1">
                                                <i className="fa fa-plus"></i>
                                            </a>
                                        </div>
                                        <div className="portfolio-info">
                                            <h3>Price {rooms.price}</h3>
                                            <p>Status {rooms.status}</p>
                                            <button onClick={() => {
                                                handleBooking(rooms.id)
                                            }}>Booking
                                            </button>
                                        </div>
                                    </div>
                                </div>)
                            }))}
                        </div>
                    </div>
                </div>
            </section>
            {showBooking ? <Booking rooomID={idRoom}/> : null}
        </>
    );
}

export default Rooms;


function Booking(props) {
    const loc =useLocation()
    const dispatch=useDispatch()
    const [start, setStart] = useState(null)
    const [end, setEnd] = useState(null)
    const [check, setCheck] = useState(false)
    const handleChange = (e) => {
        if (e.target.name === "Start") {
            setStart(e.target.value)

        }
        if (e.target.name === "End") {
            setEnd(e.target.value)
        }
    }
    const handleClick=()=>{
        let hotelID=loc.pathname.split("/")
        let booking={
            service:check, startDay:start, endDay:end, hotelId:hotelID[2], roomId:props.rooomID
        }
        dispatch(getRoomsBooking(booking))
    }

    return (
        <StyledBlockBooking>
            <StyledTitleH3>Booking</StyledTitleH3>
            <StyledBlockDiv>
                <StyledBookingContainer>
                    <StyledBookingSpan>Start Date</StyledBookingSpan>
                    <StyledBookingInput type="date" id="start" name="Start" onChange={(e) => {
                        handleChange(e)}}/>
                </StyledBookingContainer>
                <StyledBookingContainer>
                    <StyledBookingSpan>End Date</StyledBookingSpan>
                    <StyledBookingInput type="date" id="end" name="End" onChange={(e) => {
                        handleChange(e)}}/>
                </StyledBookingContainer>
                <StyledBookingContainer>
                    <StyledBookingInputCheck type="checkbox" id="check" value={check}/>
                    <StyledBookingSpan>Amen inch</StyledBookingSpan>
                </StyledBookingContainer>
            </StyledBlockDiv>
            <StyledBookingButton onClick={()=>{handleClick()}}>Booking</StyledBookingButton>
        </StyledBlockBooking>
    );
}

