import React, {useEffect, useState} from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import {useNavigate, useParams} from "react-router-dom";
import BestRooms from "../components/rooms/BestRooms";
import Wrapper from "../components/Wrapper";
import AboutMap from "../components/about/AboutMap";
import WhatWeOffer from "../components/about/WhatWeOffer";
import TouchSection from "../components/about/TouchSection";
import {useDispatch, useSelector} from "react-redux";
import {getFoolDataHotel} from "../store/action/hotels";
import {StyledContainer, StyledTitle} from "../components/styledComponents/StyledMain";
import {
    StyledContainerText,
    StyledContainerTitle,
    StyledHotelTitle
} from "../components/styledComponents/StyledComponentsAbout";

function About(props) {
    const params=useParams()
    const dispatch=useDispatch()
    let Hotel=useSelector(store=>store.hotels.hotelData)
    let photoHotel=useSelector(store=>store.hotels.photoHotel)
   useEffect(()=>{
       dispatch(getFoolDataHotel(params.hotelID))
       window.scrollTo({ top: 0, behavior: 'smooth' });
   },[])
    let hotel=Hotel.length!==0?Hotel[0]:{}
    if(hotel.photo){
        hotel.img=hotel.photo.url

    }else {
        hotel.img=''
    }
    return (<Wrapper>
            <StyledContainer>
                <Header img={hotel.img} name={hotel.hotelName}/>
                    <StyledContainer key={hotel.id} >
                        <StyledHotelTitle>
                            <StyledContainerTitle>{hotel.hotelName}</StyledContainerTitle>
                            <StyledContainerText>{hotel.description}</StyledContainerText>
                        </StyledHotelTitle>
                    </StyledContainer>
                    <AboutMap location={hotel.location}/>
                    <BestRooms rooms={hotel.room||[]}/>
                    <WhatWeOffer/>
                    <TouchSection/>
                    <Footer/>
            </StyledContainer>
        </Wrapper>);
}
export default About;