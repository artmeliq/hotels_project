import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import UserSvg from "../components/svg/UserSvg";
import {StyledBlockUser, StyledContainerUser} from "../components/styledComponents/StyledUser";
import TableBooking from "../components/Booking/TableBooking";
import {getYourBooking} from "../store/action/rooms";


function User(props) {
    const dispatch=useDispatch()
    const [showTable,setShowTable]=useState(false)
    const bookingList=useSelector(store=>store.rooms.YourBooking)
    const navigator=useNavigate()
    let userData = props.userData
    useEffect(()=>{
        if (bookingList.length===0){
            dispatch(getYourBooking())
        }
    })
    const handleClick = () => {
    }
    const handleShowBooking=()=>{
        setShowTable(!showTable)
    }

    return (
        <div className="container d-flex justify-content-center mt-5" style={{
            position: 'absolute',
            marginLeft: "-169px",
            width: 300
        }}

             onClick={handleClick()}>
            <StyledBlockUser>
                <StyledContainerUser>
                    <UserSvg/>
                    <div className="ml-3" style={{marginLeft:10}}>
                        <h5 className="name">{userData.name + ' ' + userData.surName} </h5>
                        <p className="mail">{userData.email}</p>
                    </div>
                </StyledContainerUser>
                    <div className="d-flex flex-column text-right mr-2" style={{textAlign: 'left'}}><span
                        className="amount"><p><a href="tel:${user_Data.phone}">{userData.phone}</a></p></span></div>
                <div  style={{cursor:"pointer"}} className="recent-border mt-4"><span className="recent-orders" onClick={()=>{
                    navigator("/Update-Password")
                }}>Password</span></div>
                <div style={{cursor:"pointer"}} className="wishlist-border pt-2"><span className="wishlist" onClick={()=>{
                handleShowBooking()}
                }>Your Booking</span></div>
            </StyledBlockUser>
            <TableBooking open={showTable} closeModal={handleShowBooking}/>
        </div>
    );
}

export default User;