import React from 'react';
import Search from "../components/search/Search";
import Hotel from "../components/Home/Hotel"
import {
    StyledContainer,
    StyledContainerMain,
    StyledFeatures,
    StyledTitle
} from "../components/styledComponents/StyledMain";

function Main(props) {
    return (
        <StyledContainer>
            <StyledContainerMain>
                <Search/>
                <StyledTitle>самые популярны по  недель</StyledTitle>
                <StyledFeatures>
                 <Hotel />
                </StyledFeatures>
            </StyledContainerMain>
        </StyledContainer>
    );
}

export default Main;