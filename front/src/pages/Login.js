import React from 'react';
import Wrapper from "../components/Wrapper";
import Login_Block_Rigth from "../components/Login/Login_Block_Rigth";
import Login_Block_Left from "../components/Login/Login_Block_Left";
import {useLocation} from "react-router-dom";
import {StyledContainer} from "../components/styledComponents/StyledLogin";

function Login(props) {
  const loc = useLocation()
  let key=""
  if(loc.pathname==="/login"){
    key="login"
  }else {
    key="password"
  }


  return (

    <Wrapper login={key}>
      <StyledContainer className="box-form">
        <Login_Block_Left login={key}/>
        <Login_Block_Rigth login={key}/>
      </StyledContainer>
    </Wrapper>

  );
}

export default Login;