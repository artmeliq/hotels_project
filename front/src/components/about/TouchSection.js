import React from 'react';

function TouchSection(props) {
  return (
 <>
   <div className="touch-section">
     <div className="container">
       <h3>get in touch</h3>
       <div className="touch-grids">
         <div className="col-md-4 touch-grid">
           <h4>address</h4>
           <h5>Gold Plaza</h5>
           <p>9870St Vincent Place,</p>
           <p>Glasgow, DC 45 Fr 45.</p>
           <p>United Kingdom</p>
         </div>
         <div className="col-md-4 touch-grid">
           <h4>Sales</h4>
           <h5>Sales Enquiries</h5>
           <p>Telephone : +1 800 603 6035</p>
           <p>Fax : +1 800 889 9898</p>
           <p>E-mail : <a href="mailto:example@mail.com"> example@mail.com</a></p>
         </div>
         <div className="col-md-4 touch-grid">
           <h4>general</h4>
           <h5>Gold Plaza</h5>
           <p>Telephone : +1 800 603 6035</p>
           <p>Fax : +1 800 889 9898</p>
           <p>E-mail : <a href="mailto:example@mail.com"> example@mail.com</a></p>
         </div>
         <div className="clearfix"></div>
       </div>
     </div>
   </div>
 </>
  );
}

export default TouchSection;