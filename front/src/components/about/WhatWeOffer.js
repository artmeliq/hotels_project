import React from 'react';

function WhatWeOffer(props) {
  return (
    <>
      <div className="whatweoffer">
        <div className="container">
          <h3>What We Offer</h3>
          <div className="offer-grids">
            <div className="col-md-7 offer-grid">
              <h4>Donec leo tellus, porta sit amet auctor id, porttitor</h4>
              <p>Donec leo tellus, porta sit amet auctor id, porttitor scelerisque neque. Mauris varius a
                massa eu
                fringilla. Duis tempus lectus pharetra dui posuere eleifend ut et nibh. Proin finibus
                libero sed
                augue cursus, non fermentum purus pellentesque. Praesent vel mauris mauris. Lorem ipsum
                dolor
                sit amet, consectetur adip.</p>
              <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec ut nunc sagittis
                velit
                hendrerit gravida. Proin velit risus, dapibus in purus at, ultricies suscipit neque.
                Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Fusce consequat diam et nulla commodo
                tincidunt
                eget vel massa. Sed mattis ex id orci dictum dictum</p>
            </div>
            <div className="col-md-5 offer-grid1">
              <h4>оказание услуг:</h4>
              <div className="progress">
                <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="90"
                     aria-valuemin="0" aria-valuemax="100" style={{width: "90%"}}>
                  <span className="sr-only">40% Complete (success)</span>
                </div>
              </div>
              <h4>Lacinia fermentum</h4>
              <div className="progress">
                <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="20"
                     aria-valuemin="0" aria-valuemax="100" style={{width: "80%"}}>
                  <span className="sr-only">20% Complete</span>
                </div>
              </div>
              <h4>Aenean nec eros luctus</h4>
              <div className="progress">
                <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60"
                     aria-valuemin="0" aria-valuemax="100" style={{width: "70%"}}>
                  <span className="sr-only">60% Complete (warning)</span>
                </div>
              </div>
              <h4>Vestibulum ante faucibus orci</h4>
              <div className="progress">
                <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80"
                     aria-valuemin="0" aria-valuemax={100} style={{width: "60%"}}>
                  <span className="sr-only">80% Complete (danger)</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default WhatWeOffer;