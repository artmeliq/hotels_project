import React from 'react';

function AboutMap(props) {
  return (
    <div className="why-choose">
      <div className="container">
        <h3>местоположение</h3>
        <div className="choose-grids">
          <div className="mapouter">
            <div className="gmap_canvas">
              {props.location!==undefined?  <iframe
                src={props.location}
                width="600" height="450" style={{border:0}} allowFullScreen="" loading="lazy"/>:''}
              <a href="https://kokagames.com/">Koka Games</a></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutMap;

