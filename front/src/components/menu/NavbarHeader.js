import React, {useState} from 'react';
import {Link} from "react-router-dom";

function NavbarHeader(props) {
  return (
 <>
   <div className="navbar-header">
     <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
             data-target="#bs-example-navbar-collapse-1" aria-expanded="false" >
       <span className="sr-only">Toggle navigation</span>
       <span className="icon-bar"></span>
       <span className="icon-bar"></span>
       <span className="icon-bar"></span>
     </button>
     <div className="navbar-brand">
       <h1><Link to={'/'}>hospelry</Link> </h1>
     </div>
   </div>
 </>
  );
}

export default NavbarHeader;