import React, {useState} from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";

function AppBarRoom(props) {
  const [showMenu,setShowMenu]=useState(false)
  const handleOpenMenu= ()=>{
    setShowMenu(!showMenu)
  }
  return (
    <>
      <li key={props.id} className="dropdown">
        <Link to={props.path} className="dropdown-toggle" data-toggle="dropdown" role="button"
              aria-haspopup="true" aria-expanded="false">{props.Name}<span className="caret"></span></Link>
        <ul className="dropdown-menu">
          <li><Link to={props.path}>Single Rooms</Link></li>
          <a href="#" className="dropdown-toggle" onClick={() => handleOpenMenu()} data-toggle="dropdown" role="button"
             aria-haspopup="true" aria-expanded="false">{props.Name}
            <span className="caret"></span></a></ul>
        {showMenu ? <ul className="dropdown-menu" style={{display: "block"}}>
          <li><a href="rooms.html">Single Rooms</a></li>
          <li><a href="rooms.html">Double Rooms</a></li>
          <li><a href="rooms.html">Suites Rooms</a></li>
        </ul> : null}
      </li>
    </>
  );
}
AppBarRoom.propTypes = {
  Name:PropTypes.string,
  handleOpenMenu:PropTypes.func,
};

export default AppBarRoom;