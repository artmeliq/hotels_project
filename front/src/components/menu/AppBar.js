import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import {StyledLi, StyledLink} from "../styledComponents/StyledComponentsAbout";

function AppBar(props) {
  return (

      <StyledLi key={props.id} className={props.hover?"active":''}
            onClick={(event)=>
            {props.handleClick(event)}}>
        <StyledLink to={props.path}>{props.Name}</StyledLink>
      </StyledLi>

  );
}
AppBar.propTypes = {
  hover: PropTypes.bool,
  Name:PropTypes.string
};

export default AppBar;