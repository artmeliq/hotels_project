import React from 'react';

function HeaderTop(props) {
  return (
  <>
    <div className="container">
      <div className="header-top">
        <nav className="navbar navbar-default">
          <div className="container-fluid">
            {props.children}
          </div>
        </nav>
      </div>
    </div>
    <div className="slider">
      <div className="callbacks_container">
        <ul className="rslides" id="slider">
          <li>
            <h3>great choice of <span > {props.name}</span></h3>
          </li>
        </ul>
      </div>
    </div>
  </>
  );
}

export default HeaderTop;