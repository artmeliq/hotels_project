import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getDeleteHotel, getHotelUser} from "../../store/action/hotels";
import _ from "lodash"
import iconData from "../../data/icon.json"
import TrashSvg from "../svg/TrashSvg";
import PenSvg from "../svg/PenSvg";
import RoomsSvg from "../svg/RoomsSvg";

function MyHotel(props) {
  const hotels=useSelector(store=>store.hotels.hotel_user)
  const dispatch=useDispatch()
  const [icon,showIcon]=useState(false)
  useEffect(()=>{
    dispatch(getHotelUser())
  },[])

  const handleButton=(e)=>{
    console.log(e.target.value)
    if (e.target.value==="Rooms"){
      props.hotelId(e.target.id)
    }
  }
  const handleDelete=(id)=>{
   if( window.confirm("Delete this Hotel ?")===true){
     dispatch(getDeleteHotel(id))
     for (let i = 0; i <hotels.length ; i++) {
       if (hotels[i].id===id){
         hotels.splice(i,1)
       }
     }
     getHotelUser()
   }
  }
  const handleUpdate=(id)=>{
    for (let i = 0; i <hotels.length ; i++) {
      if(hotels[i].id===id){
        props.hotelUpdate(hotels[i])
        props.setUpdate(true)
        props.room(null)
      }
    }
  }
  const handleRoom=(id)=>{
    props.room(id)
    props.setUpdate(false)
  }
  hotels.splice(3,10)
  const handleMouseEnter=(e)=>{
    for (let i = 0; i <hotels.length ; i++) {
      if (hotels[i].id===+e.target.id){
        hotels[i].icon=true
      }
      else {
        hotels[i].icon=false
      }
    }
    showIcon(true)
  }
  const handleMouse=(e)=>{
    showIcon(false)
  }

  return (
    <div style={{display:"flex",flexDirection: "column",width:"25%",margin:"0 5%"}} >
      {_.map(hotels,(hotels=>{
       return(
         <div id="grid" key={hotels.id}>
           <div className="col-md-3 col-sm-4 col-xs-6" data-groups='["webdesign"]' style={{width:"100%"}}>
             {icon&&hotels.icon?<>
               <span id={hotels.id} onMouseEnter={(e)=>{handleMouseEnter(e)}} className="icon1" onClick={()=>{handleDelete(hotels.id)}} style={{width:30,height:30,color:"red",position:"absolute",right:0,top:10,cursor:"pointer",zIndex:1000,fontSize:20,}}>
               <RoomsSvg/>
             </span>
                   <span onMouseEnter={(e)=>{handleMouseEnter(e)}}   onClick={()=>{handleUpdate(hotels.id)}} style={{width:30,height:30,color:"green",position:"absolute",right:0,top:"50%",cursor:"pointer",zIndex:1000}}>
              <TrashSvg/>
             </span>
               <span onMouseEnter={(e)=>{handleMouseEnter(e)}} onClick={()=>{handleRoom(hotels.id)}} style={{width:30,height:30,color:"green",position:"absolute",right:0,bottom:0,cursor:"pointer",zIndex:1000,fontSize:20,}}>
               <PenSvg/>
               </span></>:null}
             <div id={hotels.id}   className="portfolio-item" onMouseEnter={(e)=>{handleMouseEnter(e)}} onMouseLeave={(e)=>handleMouse(e)}>
               <div  id={hotels.id}  className="portfolio-item-thumb">
                 <img  src={"http://localhost:4000"+hotels.photo[0].url} alt="" className="img-res"/>
                 <a href="#" className="rectangle" data-toggle="modal" data-target="#portfolioItem1">
                   <i className="fa fa-plus"/>
                 </a>
               </div>
               <div className="portfolio-info "style={{backgroundColor:"white", display:"flex",flexDirection: "column"}}>
                 <h3>{hotels.hotelName}</h3>
                 <h5>{hotels.address}</h5>
               </div>
             </div>
           </div>
         </div>
       )
      }))}
    </div>
);
}

export default MyHotel;