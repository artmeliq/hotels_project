import React, {useState} from 'react';
import _ from "lodash"
import {Link, useLocation, useNavigate} from "react-router-dom";
import Slide from "../Slide/Slide";


function BestRooms(props) {
    let room=props.rooms ||[]
    const [slide,SetSlide]=useState(false)
    const [slideImg,SetSlideImg]=useState([])
    const handleSlide=(id)=>{
        let slider=room.filter(room=>room.id===id)
        SetSlideImg(slider[0].photo)
        SetSlide(true)
        window.scroll(0,1000)
    }
  return (
      <>
    <div className="ourteam">
      <div className="container">
          <h3>The best hotel rooms</h3>
          <div  className="team-grids">
          {_.map(room,(room)=>{
              return(
                      <div key={room.id} style={{cursor:'pointer'}}
                           onClick={(e)=>{handleSlide(room.id)}}
                           className="col-md-3 col-sm-4 col-xs-6" data-groups='["webdesign"]'>
                          <div   className="portfolio-item">
                              <div className="portfolio-item-thumb">
                                  <img  src={'http://localhost:4000'+room.photo[0].url.replaceAll("\\","\\\\")} alt="" className="img-res"/>
                                  <a href="#" className="rectangle" data-toggle="modal" data-target="#portfolioItem1">
                                      <i className="fa fa-plus"></i>
                                  </a>
                              </div>
                              <div className="portfolio-info">
                                  <h3>Price {room.price}</h3>
                                  <p>Status</p>
                              </div>
                          </div>
                      </div>
                  )
          })
          }
          </div>
          <Link style={{float:"right",fontSize:20,color:"black",}} to='rooms'>All or rent</Link>
      </div>
    </div>
    {slide?<Slide photo={slideImg} slideShow={SetSlide}/>:null}
    </>
  );
}

export default BestRooms;