import React, {useEffect} from 'react';
import {useNavigate} from "react-router-dom";

function Wrapper(props) {
  const navigate = useNavigate()
  const login = props.login
  const token = localStorage.getItem('token')

  useEffect(() => {

    if (token === undefined || token === '' || token === null) {
      navigate('/login')
    } else if (token !== '' && login === 'login') {
      navigate('/')
    }
  }, [])
  return (
    <>
      {props.children}
    </>

  );
}

export default Wrapper;