import React, {useEffect, useState} from 'react';
import _ from "lodash"

function Slide(props) {
    const photo=props.photo
    const [imgList,setImgList]=useState([])
    const [img,SetImg]=useState(photo[0])
    const [limit,setLimit]=useState(0)
    useEffect(()=>{
        handleLimit(0)
    },[photo])
    const handleLimit=(_limit)=>{
        let slide=[]
        for (let i = _limit; i<_limit+8 ; i++) {
            if (!photo[i]){break}
            slide.push(photo[i])
        }
        setImgList(slide)
    }
    const handleImg=(e)=>{
        if(imgList.length===1){
            setLimit(0)
            handleLimit(0)
        }
       else if (+e.target.id===imgList[imgList.length-1].id){
            setLimit(limit+8)
            handleLimit(limit+8)
        }
        else if(e.target.id===imgList[0].id){
            if (e.target.id===imgList[imgList.length-1]){
                setLimit(limit-8)
                handleLimit(limit-8)
            }
        }
        for (let i = 0; i <imgList.length ; i++) {
            if (imgList[i].id===+e.target.id){
                imgList[i].hover=true
            }else {imgList[i].hover=false}
        }
        e.stopPropagation()
        let img=photo.filter(item=>item.id===+e.target.id)[0]
        SetImg(img)
    }

    return (
       <div style={{position:'absolute',width:window.innerWidth-25,height:window.innerHeight-5,top:1000}}
       onClick={()=>{props.slideShow(false)}}>
           <div className="fon"></div>
           <div className="blockImg">
               <div  className="img_slide" style={{top:100,opacity:1}}>
                   <img className='_img' src={"http://localhost:4000"+img?.url.replaceAll('\\','\\\\')} alt=""/>
               </div>
               <div className="blockList">
                   {_.map(imgList,(photo=>{
                       return(
                           <div key={photo.id}  className="img_one">
                               <img id={photo.id} onClick={(e)=>{handleImg(e)}} className={(photo.hover)?'img_active img-list':'img-list'} src={"http://localhost:4000"+photo.url.replaceAll('\\','\\\\')} alt=""/>
                           </div>
                       )
                   }))}
               </div></div>
       </div>
    );
}

export default Slide;