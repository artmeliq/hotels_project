import React, {useEffect} from 'react';
import Modal from 'react-modal';
import _ from "lodash"
import moment from "moment"
import {
    StyledDivTable,
    StyledH2Booking,
    StyledTableBooking, StyledTableTd,
    StyledTableTh,
    StyledTableTr, StyledTableTr2
} from "../styledComponents/StyledBookin";
import {useSelector} from "react-redux";
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -80%)',
    },
};

function TableBooking(props) {
    let subtitle;

    function afterOpenModal(){

    }


    return (
        <div>
            <Modal
                isOpen={props.open}
                onAfterOpen={afterOpenModal}
                onRequestClose={props.closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <StyledDivTable>
                    <StyledH2Booking>MY BOOKING</StyledH2Booking>
                    <StyledTableBooking>
                        <StyledTableTr>
                            <StyledTableTh>Hotel Name</StyledTableTh>
                            <StyledTableTh>Hotel Address</StyledTableTh>
                            <StyledTableTh>Room Count</StyledTableTh>
                            <StyledTableTh>Room Price</StyledTableTh>
                            <StyledTableTh>Status</StyledTableTh>
                            <StyledTableTh>Start Date</StyledTableTh>
                            <StyledTableTh>End Date</StyledTableTh>
                        </StyledTableTr>
                        <Booking/>
                    </StyledTableBooking>
                </StyledDivTable>
            </Modal>
        </div>
    );
}
export default TableBooking
function Booking (props){
    const bookingList=useSelector(store=>store.rooms.YourBooking)
    console.log(bookingList)
        return(<>
            {_.map(bookingList,(booking=>{
                return(
                    <StyledTableTr2 id={booking.id} key={booking.id}>
                        <StyledTableTd>{booking.hotels.hotelName}</StyledTableTd>
                        <StyledTableTd>{booking.hotels.address}</StyledTableTd>
                        <StyledTableTd>{booking.roomBooking.roomCount}</StyledTableTd>
                        <StyledTableTd>{booking.roomBooking.price}</StyledTableTd>
                        <StyledTableTd>{booking.service?"Amen inch":"sovorakan"}</StyledTableTd>
                        <StyledTableTd>{moment(booking.startDay).format('L')}</StyledTableTd>
                        <StyledTableTd>{moment(booking.endDay).format('L')}</StyledTableTd>
                    </StyledTableTr2>
                )
                }))}
            </>
    )
}