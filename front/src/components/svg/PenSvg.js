import React from 'react';
import iconData from "../../data/icon.json";

function PenSvg(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor"
         className="bi bi-hospital" viewBox="0 0 16 16">
      <path d={iconData.room.d1}/>
      <path d={iconData.room.d2}/>
    </svg>
  );
}

export default PenSvg;