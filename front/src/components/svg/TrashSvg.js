import React from 'react';
import iconData from "../../data/icon.json";

function TrashSvg(props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor"
         className="bi bi-pencil-fill" viewBox="0 0 16 16">
      <path d={iconData.pen.d}/>
    </svg>
  );
}

export default TrashSvg;