import {toast} from "react-toastify";
export function error (text){
    toast.error(text, {
        position: "bottom-right",
        theme:"colored",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}

export function success (text){
    toast.success(text, {
        position: "bottom-right",
        theme:"colored",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}
export function UserHello (text){
    toast.info(text, {
        theme:"colored",
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
}