import React from 'react';
import iconData from "../../data/icon.json";

function RoomsSvg(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" className="bi bi-trash"
      viewBox="0 0 16 16">
      <path
        d={iconData.trash.d1}/>
      <path fill-rule="evenodd"
            d={iconData.trash.d2}/>
    </svg>
  );
}

export default RoomsSvg;