import React from 'react';
import _ from "lodash";

function HotelPagination(props) {
    return (
        <>
            <ul className="PaginationHotel">
                <li className="previous disabled" onClick={props.handlePageClick}>
                    <a className=" " tabIndex="-1"
                       role="button" aria-disabled="true"
                       aria-label="Previous page" rel="prev">&lt; </a></li>
                {_.range(1, props.pageCount+1).map((r) => (
                    <li key={r} onClick={props.handlePageClick}><a rel="next" role="button" tabIndex="0" aria-label="Page 2">{r}</a></li>
                ))}
                <li className="next" onClick={props.handlePageClick}>
                    <a className="" tabIndex="0" role="button"
                       aria-disabled="false"
                       aria-label="Next page"
                       rel="next"> &gt;</a></li>
            </ul>
        </>
    );
}

export default HotelPagination;