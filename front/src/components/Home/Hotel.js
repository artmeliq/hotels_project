import React, {useEffect, useState} from 'react';
import _ from "lodash"
import FOOLHOTELS from "../../data/FoolHotels.json"
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getDataHotels} from "../../store/action/hotels";
import HotelPagination from "./HotelPagination";

function Hotel(props) {
  const dispatch=useDispatch()
  const FoolHotels=useSelector(store=>store.hotels.Data_Hotels)
  const region=useSelector(store=>store.hotels.region)
  const pageCount=useSelector(store=>store.hotels.pageCount)
  const [page,setPage]=useState(1)
  const navigate=useNavigate()
  const handleClickHotel = (id) => {
    navigate('/hotel/' + id)
  }
  useEffect(() => {
    dispatch(getDataHotels(page,null))
  },[])
  const handlePageClick = (event) => {
    console.log(event.target.outerText)
    let _page
    if (event.target.outerText==='<'){
      if(page>=1){
        setPage(page-1)
        _page=+page-1
      }
    }
    else if(event.target.outerText===">"){
      if(page<=pageCount){
        setPage(page+1)
        _page=+page+1
      }
    }
    else {
      setPage(event.target.outerText)
      _page=event.target.outerText
    }
    console.log(_page)
    if (region===null){
      dispatch(getDataHotels(_page,null))
    }else {
      dispatch(getDataHotels(_page,region))
    }

  };
  return (
      <>
        {_.map(FoolHotels,(item)=>{
        return(  <div key={item.id} className="col-md-3 feature-grid">
            <div className="feature menuHotel" onClick={()=>{handleClickHotel(item.id)}}>
              <div className="feature1">
                <div>
                  <img className="img_hotels" src={'http://localhost:4000'+ item.photo[0].url} alt={item.hotelName}/>
                </div>
                <h4>{item.hotelName}</h4>
              </div>
              <div className="feature2">
                <p style={{overflow: "hidden",maxHeight:"170px"}}>{item.description}</p>
              </div>
            </div>
          </div>
        )
        })}
        <HotelPagination pageCount={pageCount} handlePageClick={handlePageClick}/>
    </>
  );
}

export default Hotel;


