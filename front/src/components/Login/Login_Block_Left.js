import React from 'react';
import {useNavigate} from "react-router-dom";
import Helmet from 'helmet'
import {useDispatch} from "react-redux";
import {googleCookie} from "../../store/action/register";
import {
    StyledLoginA,
    StyledLoginH1,
    StyledLoginLeft,
    StyledLoginOverly,
    StyledLoginP,
    StyledLoginWith,
    StyledPLogin,
    StyledSpanLogin
} from "../styledComponents/StyledLogin";
import ArrowSvg from "../svg/ArrowSvg";


function LoginBlockLeft(props) {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const handleGoogle = (e) => {
        e.preventDefault();
        const REACT_APP_GOOGLE_CLIENT_ID = '737846858998-viecltm50rvrdn95s4vcfarl6jk62ug1.apps.googleusercontent.com';
        const {gapi} = window;
        gapi?.load('auth2', () => {
            gapi.auth2.authorize({
                client_id: REACT_APP_GOOGLE_CLIENT_ID,
                scope: [
                    'https://www.googleapis.com/auth/userinfo.email',
                    'https://www.googleapis.com/auth/userinfo.profile',
                ].join(' '),
                response_type: 'id_token permission'
            }, (response) => {
                if (response.error) {
                    return;
                }
                const {access_token: accessToken} = response
                dispatch(googleCookie(accessToken))
            });
        });
    }
    return (
        <StyledLoginLeft>
            <StyledLoginWith onClick={() => {navigate('/')}} className="with">
                <ArrowSvg/>
            </StyledLoginWith>
            {props.login === "login" ? <>
                    <Helmet>
                        <script async defer src="https://apis.google.com/js/api:client.js"/>
                        <script async defer src="https://connect.facebook.net/en_US/sdk.js"/>
                    </Helmet>
                    <StyledLoginOverly>
                        <StyledLoginH1>FAST HOTEL</StyledLoginH1>
                        <p>Quick և Easy to choose the hotel convenient for you ․ Place the order right from home</p>
                        <StyledSpanLogin>
                            <StyledLoginP>login with social media</StyledLoginP>
                            <StyledLoginA href="#" onClick={handleGoogle}>
                                <i className="fa fa-facebook" aria-hidden="true"/>Google</StyledLoginA>
                            <StyledLoginA href="#">
                                <i className="fa fa-twitter" aria-hidden="true"/> Login with facebook</StyledLoginA>
                        </StyledSpanLogin>
                    </StyledLoginOverly></> :
                <StyledLoginOverly>
                    <StyledLoginH1>FAST HOTEL</StyledLoginH1>
                    <StyledPLogin>Quick և Easy to choose the hotel convenient for you ․ Place the order right from
                        home</StyledPLogin>
                </StyledLoginOverly>}
        </StyledLoginLeft>
    );
}

export default LoginBlockLeft;
