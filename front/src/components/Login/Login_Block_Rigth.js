import React, {useEffect, useState} from 'react';
import {Link, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getLoginUser} from "../../store/action/login";
import Password from "./Password";
import {error} from "../svg/Popup";

function LoginBlockRight(props) {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  let userData = useSelector(store => store.login.loginToken);
  const [login, setLogin] = useState({email: '', password: ''})
  const [errorEmail, setErrorEmail] = useState(false)
  const [checked,setChecked]=useState(false)
  const [errorPassword, setErrorPassword] = useState(false)
  const [errorData, setErrorData] = useState(false)



  const handleLogin = () => {
    dispatch(getLoginUser(login.email, login.password,(err,data)=>{
      if (err){
        if(data.errors){
          setErrorData(data.errors)
          if(data.errors.email){
            error(data.errors.email[0])
            setErrorEmail(true)
          }

          if (data.errors.password){
            error(data.errors.password[0])
            setErrorPassword(true)
          }
        }
      }
    }))
  }
  useEffect(() => {
    if (userData !== undefined) {
      localStorage.setItem('token', userData)
      navigate('/')
    }

  }, [userData])


  const handleChange = (e) => {
    if(e.target.name==='email'&& errorEmail===true){
      setErrorEmail(true)
    }
    if (e.target.name==='password'&& errorPassword===true){
      setErrorPassword(true)
    }
    login[e.target.name] = e.target.value
  }
  return (
   <>{props.login==="login"?<>
     <div className="right">
       <h5>Login</h5>
       <p style={{paddingTop: 45}}>Don't have an account? <Link to='/register'>Creat Your Account</Link> it takes
         less than a minute</p>
       <div className="inputs">
         <input type="text" placeholder="user email" name='email' onChange={(e) => {
           handleChange(e)
         }}/>
         {errorEmail?<span className="errorLogin">{errorData.email[0]}</span>:null}
         <input type={checked?"text":"password"} name='password' placeholder="password" onChange={(e) => {
           handleChange(e)
         }}/>
         {errorPassword? <span className="errorLogin">{errorData.password[0]}</span>:null}
       </div>


       <div className="remember-me--forget-password">
         <label>
           <input type="checkbox" name="item" checked={checked} onChange={()=>{
             setChecked(!checked)
           }}/>
           <span className="text-checkbox">See password?</span>
         </label>

       </div>

       <button onClick={(e) => {
         handleLogin(e)
       }}>Login
       </button>
     </div>
   </>:<Password/>}

   </>
  );
}

export default LoginBlockRight;
