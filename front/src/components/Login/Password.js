import React, {useState} from 'react';
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {getUpdatePassword} from "../../store/action/login";

function Password(props) {
    const dispatch=useDispatch()
  const [checked,setChecked]=useState(false)
    const [password,setPassword]=useState({
        password:'',
        newPassword:''
    })
  const handleChange=(e)=>{
    const name=e.target.name
      password[name]=e.target.value
  }
  const handleNwePassword=()=>{
        dispatch(getUpdatePassword(password))

  }
  return (
   <div className="right">
   <h5>Update </h5>
   <div className="inputs">
     <input type="text" placeholder="Password" name='password' onChange={(e) => {
       handleChange(e)
     }}/>
     <input type={checked?"text":"password"} name='newPassword' placeholder="New password" onChange={(e) => {
       handleChange(e)
     }}/>
   </div>
   <div className="remember-me--forget-password">
     <label>
       <input type="checkbox" name="item" checked={checked} onChange={()=>{
         setChecked(!checked)
       }}/>
       <span className="text-checkbox">See Password</span>
     </label>
   </div>

   <button onClick={(e) => {
     handleNwePassword(e)
   }}>Update
   </button>
   </div>
  );
}

export default Password;