import React, {useEffect, useState} from 'react';
import _ from "lodash"
import {useDispatch, useSelector} from "react-redux";
import {getArmeniaMap, getDataHotels} from "../../store/action/hotels";

function Search(props) {
  const dispatch=useDispatch()
  let region = useSelector(store => store.hotels.map_Armenia);
  const handleMap = (event) => {
    dispatch(getDataHotels(1,event.target.id))
  }
  useEffect(()=>{
    if (region.length===0){
      dispatch(getArmeniaMap())
    }
  },[])
  return (
    <div>
      <svg id="armeniamap" data-name="armeniamap" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 656.42 669.86">
        <defs>

        </defs>
        <title>Armenia map</title>
        {
          _.map(region, (item => {
            return (
              <path key={item.id} id={item.id} onClick={(event) => {
                handleMap(event)
              }} className={item.className + ' region'} data-url={item.dataUrl}
                    d={item.d}
                    transform="translate(-15.04 -49.81)">l</path>
            )
          }))
        }

        <text x="80%" y="80%" data-url="syunik" className="small">Сюник</text>
        <text x="50%" y="20%" data-url="tavush" className="small">Тавуш</text>
        <text x="28%" y="17%" data-url="lori" className="small">Лори</text>
        <text x="8%" y="17%" data-url="shirak" className="small">Ширак</text>
        <text x="8%" y="39%" data-url="aragacotn" className="small">Арагацотн</text>
        <text x="11%" y="49%" data-url="armavir" className="small">Армавир</text>
        <text x="33%" y="40%" data-url="kotayk" className="small">Котайк</text>
        <text x="35%" y="57%" data-url="ararat" className="small">Арарат</text>
        <text x="49%" y="45%" data-url="gegharkunik" className="small">Гегаркуник</text>
        <text x="53%" y="62%" data-url="vayots-dzor" className="small">Вайоц Дзор</text>
      </svg>
    </div>
  );
}

export default Search;