import styled from 'styled-components';

export const StyledBookingSpan = styled.span`
   font-size: 18px;
    padding: 0 10px;
`;
export const StyledBookingContainer = styled.div`
   display:flex;
  justify-content: space-around;
  padding-top: 10px;
  
`;
export const StyledBookingInput = styled.input`
`;
export const StyledBookingInputCheck = styled.input`
    width: 20px;
  height: 20px;
`;
export  const  StyledBookingButton=styled.button`
 margin-left : 30%;
  border:1px solid green;
  width: 100px;
  height: 30px;
  font-size: 18px;
  font-family: 'Rancho-Regular'`;

export const StyledDivTable=styled.div`
    width: 800px;
    min-height: 200px;
`;
export const StyledTableBooking=styled.table`
    margin: 0 auto;
`;
export const StyledH2Booking=styled.h2`
  color: #1d1d1d;
  text-align: center;
  font-family: 'Rancho-Regular'
`;
export const  StyledTableTr=styled.tr``;
export const  StyledTableTr2=styled.tr`
  &:hover {
    background-color: #949ca7;
    opacity: 0.9;
    cursor: pointer;
  }
`;
export const  StyledTableTh=styled.th`
    border: 1px solid black;
  padding: 5px 20px;
  text-align: center;
`;
export const  StyledTableTd=styled.td`
    border: 1px solid black;
  padding: 5px 10px;
  text-align: center;
`;
export const StyledInputBooking=styled.input`
  width: 100%;
border: none;
`;