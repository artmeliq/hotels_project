import  styled from "styled-components"

export const StyledBlockUser = styled.div`
    background-color: #fff;
    width: 280px;
    border-radius: 33px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    padding: 2rem !important
`;
export const StyledContainerUser = styled.div`
    display: flex;
    align-items: center
`;
export const StyledInput = styled.input`
    position: absolute;
`;