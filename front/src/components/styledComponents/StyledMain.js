import styled from 'styled-components';

export const StyledContainer = styled.div`
   text-align: center;
  padding:0;
  background:#eee;
`;
export const StyledFeatures = styled.div`
    margin-top: 3em;
`;
export const StyledContainerMain = styled.div`
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    
@media (min-width: 768px) {
        width: 750px;
}
@media (min-width: 992px) {
        width: 970px;
}
@media (min-width: 1200px) {
        width: 1170px;
}`;
export const StyledTitle = styled.h3``;
export const StyledTitleH3=styled.h3`
 font-family:'Rancho-Regular';
    font-size: 35px;
    padding-top: 10px;
    text-align: center;
`
export  const StyledUserLi = styled.li`
`;
export  const StyledBlockDiv = styled.div`
  margin: 0 auto
`;
export  const StyledBlockBooking = styled.div`
  width: 25%;
  height: 90%;
  position: absolute;
  top:50px;
  right: 0;
  background-color: whitesmoke;
`;
