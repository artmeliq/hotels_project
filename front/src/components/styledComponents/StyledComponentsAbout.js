import styled from 'styled-components';
import {Link} from "react-router-dom";

export const StyledContainer = styled.div``;
export const StyledContainerTitle = styled.h2``;
export const StyledContainerText = styled.p``;
export const StyledHeader = styled.div`
  background-size: cover !important;
  -webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  -ms-background-size: cover !important;
  min-height: 764px;
  padding-bottom: 4em;
  background: ${props => props.img};
  @media (max-width: 1366px) {
    min-height: 652px;
  };
  @media (max-width: 1280px) {
    min-height: 602px;
  };
  @media (max-width: 1080px) {
    min-height: 486px;
  };
  media (max-width: 991px) {
  min-height: 363px;
};
  media(max-width: 640 px) {
  min-height: 301px;
};
  media(max-width: 480 px) {
  background: url(./images/banner1.jpg) no-repeat -130px 0px;
  background-size: cover;
};
  media  (max-width: 320px) {
  .header {
    background: url(./images/banner1.jpg) no-repeat -200px 0px;
    background-size: cover !important;
    min-height: 260px;
  }
;
`;
export const StyledCollapse = styled.div`
  display: block;
  max-height: 340px;
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  box-shadow: inset 0 1px 0 rgba(255, 255, 255, .1);
  @media (min-width: 768px) {
    width: auto;
    border-top: 0;
    box-shadow: none;

`;
export const StyledUl = styled.ul`
  padding-left: 0;
  float: right;
  margin: 0;
  list-style: none;
  @media (min-width: 768px) {
    margin: 7.5px -15px;
  }
  @media (max-width: 640px) {
    float: none;
    margin: 0;
    width: 100%;
    text-align: center;
  }
`;
export const StyledLink = styled(Link)`
      color: #fff8f8;
  font-family: 'Rancho-Regular';
  font-size: 1.4em;
  position: relative;
  display: block;
  padding: 10px 15px;
  
  @media (max-width: 991px) {
    position: relative;
    display: block;
    padding: 10px 10px;
    line-height: 20px;
  }
  &:hover  {
    cursor: pointer;
    color: #fff;
    background-color: rgba(249, 255, 247, 0.43);
    text-decoration: none;
  }
  &:focus {
    color: #fff;
    text-decoration: none;
    background-color: rgba(249, 255, 247, 0.43);
  }
`;
export const StyledLi = styled.li`
  position: relative;
  display: block;
  float: left;
  &:hover {
    background-color: rgba(249, 255, 247, 0.43);
    text-decoration: none;
    cursor: pointer;
  }
`;
export  const StyledUser = styled.li``;
export  const StyledHotelTitle = styled.div`
min-height: 100px`;

