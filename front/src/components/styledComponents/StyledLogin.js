import styled from 'styled-components';

export const StyledContainer = styled.div`
  margin: 5% auto;
  width: 80%;
  background: #FFFFFF;
  border-radius: 10px;
  overflow: hidden;
  display: flex;
  flex: 1 1 100%;
  align-items: stretch;
  justify-content: space-between;
  box-shadow: 0 0 20px 6px #090b6f85;
  @media (max-width: 980px) {
      flex-flow: wrap;
      text-align: center;
      align-content: center;
      align-items: center;
  }
`;
export const  StyledLoginLeft = styled.div `
  color: #FFFFFF;
  background-size: cover;
  background-repeat: no-repeat;
  background-image: url("../images/hotels/banner1.jpg");
  overflow: hidden;
`;
export const  StyledLoginWith = styled.div `
  width: 40px;
  height: 40px !important;
  cursor: pointer;
  position: absolute;
  z-index: 999;
`;
export const  StyledLoginOverly = styled.div `
  padding: 30px;
  width: 100%;
  height: 100% !important;
  background: #5961f9ad;
  overflow: hidden;
  box-sizing: border-box;

`;
export const StyledLoginH1=styled.h1`
  font-size: 10vmax;
  line-height: 1;
  font-weight: 900;
  margin-top: 40px;
  margin-bottom: 20px;
`;
export const StyledLoginA=styled.a`
  background: #3b5998;
  color: #FFFFFF;
  margin-top: 10px;
  padding: 14px 50px;
  border-radius: 100px;
  display: inline-block;
  box-shadow: 0 3px 6px 1px #042d4657;
`;
export const StyledLoginP=styled.p`
  margin-top: 30px;
  font-weight: 900;
`;
export const StyledSpanLogin=styled.span``
export const StyledPLogin=styled.p``