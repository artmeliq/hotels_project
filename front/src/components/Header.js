import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import _ from 'lodash'
import Menu from '../data/menu.json'
import UserNav from "./menu/UserNav";
import AppBar from "./menu/AppBar";
import NavbarHeader from "./menu/NavbarHeader";
import HeaderTop from "./menu/HeaderTop";
import {useDispatch, useSelector} from "react-redux";
import {getDataUser} from "../store/action/login";
import {StyledCollapse, StyledHeader, StyledUl} from "./styledComponents/StyledComponentsAbout";
import {error, UserHello} from "./svg/Popup";


function Header(props) {
    let menu=Menu
    const [DataUser,setDataUSer]=useState(false)
    const navigate=useNavigate()
    const dispatch=useDispatch()
    let user_Data = useSelector(store => store.login.Data_User);
    let token=localStorage.getItem('token')
    if (token!=='') {
        if (!user_Data.role && !DataUser){
            dispatch(getDataUser())
            setDataUSer(true)
        }
        menu.map(item => {
            if (item.Name === 'Login') {
                delete item.hover
                item.Name = 'Logout'
            }
            else if(user_Data.role==='Admin'){
                if(menu[menu.length-1].Name!=='My Hotel'){
                    menu.push(
                        {"id": 13,
                        "Name": "My Hotel",
                        "path": "#"
                    })
                }
            }else {
                if(menu[menu.length-1].Name==='My Hotel'){
                    menu.splice(menu.length-1,1)
                }
            }
        })
    }

        const handleClick=(e)=>{
            let name=e.target.outerText
            menu.map(item=>{

                if (name===item.Name){
                    item.hover=true
                }
                if (item.Name!==name && item.hover){
                    delete (item.hover)
                }
            })
            if (name==="Login"){
                navigate('/login')
            }
            console.log(name)
            if(name==="Services"||name==="Rooms"||name==="Gallery"||name==="Contact"){
                console.log("aaaaa")
                error("Sorry page not found")
            }
            if (name==="My Hotel"){
                navigate('/register-hotel')
            }
            if(name==='Logout'){
            menu.map(item=>{
                if (item.Name==="Logout"){
                    item.Name="Login"
                    if(menu[menu.length-1].Name==="My Hotel"){
                        menu.splice(menu.length-1,1)
                    }

                    localStorage.setItem('token','')
                }
            })
            }

    }



    return (
        <StyledHeader  img={props.img?` url(http://localhost:4000//${props.img.replaceAll("\\","\\\\")}) no-repeat 0px 0px fixed `: `url(../images/hotels/banner1.jpg) no-repeat 0px 0px fixed`}>
                <HeaderTop name={props.name}>
                          <NavbarHeader />
                            <StyledCollapse id="bs-example-navbar-collapse-1">
                                <ul className="navbar-nav nav" >
                                    {_.map(menu,(item=>{
                                        if (item.Name!=='Rooms'){
                                            return <AppBar key={item.id} handleClick={handleClick} id={item.id} hover={item.hover} path={item.path} Name={item.Name}/>
                                        }
                                    }))}
                                    {token!==''? <UserNav userData={user_Data}/>:null}
                                </ul>
                            </StyledCollapse>
                </HeaderTop>
        </StyledHeader>
    );
}

export default Header;
