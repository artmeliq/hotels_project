import React from 'react';

function RegisterInput(props) {
  return (
    <div key={props.id} className=" inputMargin form-group input-group">
      <div className="input-group-prepend">
        <span className="input-group-text"> <i
          className="fa fa-user"></i> </span>
      </div>
      <input name="" className={props.registerUser[props.title].error ? 'form-control activeError' : "form-control"}
             placeholder={props.title}
             type={props.type}
             onChange={(e) => {
               props.handleClick(e)
             }}/>
    </div>
  );
}

export default RegisterInput;