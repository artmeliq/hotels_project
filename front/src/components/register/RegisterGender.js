import React from 'react';
import _ from "lodash"

function RegisterGender(props) {
  return (
    <div className="inputMargin form-group input-group" key={props.id}>
      <div className="input-group-prepend">
        <span className="input-group-text"> <i
          className="fa fa-building"></i> </span>
      </div>
      <select className={props.registerUser[props.title].error ? 'form-control activeError' : "form-control"}
              onChange={(e) => {
                props.handleSelect(e)
              }}>
        <option selected="">{props.title}</option>
          {_.map(props.option,(item=>{
              return <option  key={item}>{item}</option>
          }))}
      </select>
    </div>
  );
}

export default RegisterGender;