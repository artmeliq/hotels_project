import React, {useState} from 'react';
import GoogleLogin from "react-google-login";
import {Button} from "@mui/material";
import GoogleIcon from "@mui/icons-material/Google";

function RegisterGoogle(props) {
  const clientId = '313991288198-g7nlf72hcen4mqv1f8sh9sfcbgr7s62l.apps.googleusercontent.com'
  const [GoogleReg,setGoogleReg]=useState({})
  const onLoginSuccess = (res) => {
    setGoogleReg(res)
  }

  const onLoginFailure = () => {

  }
  return (
    <GoogleLogin
      clientId={clientId}
      buttonText="Sign In"
      render={(renderProps) => (
        <Button
          onClick={renderProps.onClick}
          disabled={renderProps.disabled}
          variant="contained"
          sx={{
            width: "100%",
            bgcolor: "#4285F4 !important",
            color: "#fff",
            padding: "12px 0",
            margin: "10px 0 25px",
          }}
        >
          <GoogleIcon sx={{ color: "#fff", marginRight: "5px" }} />
          Continue with Google
        </Button>
      )}

      onSuccess={onLoginSuccess}
      onFailure={onLoginFailure}
      cookiePolicy={"single_host_origin"}
      isSignedIn={true}
    />

  );
}

export default RegisterGoogle;