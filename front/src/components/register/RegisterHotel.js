import React, {useEffect, useState} from 'react';
import {VscLocation} from 'react-icons/vsc'
import {BiCommentDetail} from 'react-icons/bi'
import {MdArrowBack} from 'react-icons/md'
import _ from 'lodash'
import {useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {getPhotoUpload, getRegisterHotel} from "../../store/action/registerHotel";
import MyHotel from "../MyHotel/MyHotel";
import {getRoomsCreate} from "../../store/action/rooms";
import {getUpdateHotel} from "../../store/action/hotels";
import {error, success} from "../svg/Popup";




function RegisterHotel(props) {
    const navigate = useNavigate()
    const dispatch =  useDispatch()
    const [stars, setStars] = useState(6)
    const [file,setFile]=useState()
    const [update,setUpdate]=useState(false)
    const [StatusHotel,SetStatusHotel]=useState(false)
    const [hotelUpdate,setHotelUpdate]=useState({})
    const [idHotel,setIDHotel]=useState()
    const [idHotelRooms,setIdHotelRooms]=useState(null)
    let RegisterHotel = useSelector(store => store.registerHotel.data_hotel);
    let RegisterHotelStatus = useSelector(store => store.registerHotel.status);
    let region = useSelector(store => store.hotels.map_Armenia);
    const [data, setData] = useState({
        hotelName: '',
        address: '',
        description: '',
        hotelStar: '',
    })
    useEffect(()=>{
        if(RegisterHotel.id && RegisterHotel.userId && file!==undefined){
            dispatch(getPhotoUpload(RegisterHotel.id,file))
        }

    },[RegisterHotel])
    useEffect(()=>{
        if (RegisterHotelStatus===true){
            SetStatusHotel(true)
            success("Hotel Register Success")
                navigate('/')
        }

    },[RegisterHotelStatus])


    const handleSelect = (ev) => {
        setStars((ev.target.id))
        data.hotelStar = 6 - ev.target.id
    }
    const handleUpdate=()=>{
        for (const key in data) {
            if(data[key]===''){
                data[key]=hotelUpdate[key]
            }
        }
        data.hotelId=hotelUpdate.id
        dispatch(getUpdateHotel(data))
    }

    const handleChange = (ev) => {
        const name = ev.target.id
        data[name] = ev.target.value
    }
    const handleChangeRegion = (ev) => {
       data.region=ev.target.value
    }

    const handleSubmit = (ev) =>  {
        ev.preventDefault()
        if(ev.target.outerText==="Update"){
           handleUpdate()
            return
        }
        ev.preventDefault()
        dispatch(getRegisterHotel(data))
    }
    const handleFile = (e) =>  {
        setFile(e.target.files[0])
    }



    return (
        <div className='hotelRegister'>
            <MyHotel hotelId={setIDHotel} setUpdate={setUpdate} hotelUpdate={setHotelUpdate} room={setIdHotelRooms} />
            {idHotelRooms!==null?<RegisterRooms hotelID={idHotelRooms}/>: <form className="signup-form">
                <div className="form-header">
                    <div className="back">
                        <div  onClick={()=>{navigate('/')}} className="back-arrow">
                            <MdArrowBack/>
                        </div>
                    </div>
                    <div className='hotelRegister-title'>
                        <h1>Register Your Hotel</h1>
                    </div>
                </div>
                <div className='hotelRegister-form-body'>
                    <label htmlFor="hotelName" className='hotelRegister-form-body-label'>Your hotel name </label>
                    <input id='hotelName' onChange={ev => handleChange(ev)} required placeholder='name' name='hotelName'  type="text"/>
                </div>
                <div className='hotelRegister-form-body'>
                    <label htmlFor="hotelAddress" className='hotelRegister-form-body-label'>Your hotel Img </label>
                    <input
                      id='address'
                      onChange={event => handleFile(event)}
                      placeholder='' name='file'
                      type="file"
                    />
                </div>
                <div className='hotelRegister-form-body'>
                    <label htmlFor="hotelAddress" className='hotelRegister-form-body-label'>{<VscLocation
                      fontSize={30}/>} Your hotel address </label>
                    <div style={{display:"flex",width:"75%"}}>
                        <select style={{width:"35%",backgroundColor:"#1d1d1d"}} onChange={(ev)=>{
                            handleChangeRegion(ev)
                        }}>
                            {_.map(region,(item=>{
                                return <option key={item.id} value={item.id}>{item.name}</option>
                            }))}
                        </select>
                        <input id='address' onChange={ev => handleChange(ev)} placeholder='region, country, adress..' name='img' type="text"/>
                    </div>

                </div>

                <div className='hotelRegister-form-body'>
                    <label htmlFor="" className='hotelRegister-form-body-label'>
                        About your hotel {<BiCommentDetail fontSize={23}/>} </label>
                    <textarea name="description" placeholder='Example: Tell us why travelers should stay in your hotel?'
                              className='hotelRegister-form-text'
                              id="description" onChange={ev => handleChange(ev)} cols="30" rows="10"/>
                </div>

                <div className='hotelRegister-rating stars_hotel'>
                    <h1>Select your hotel rating </h1>
                    <div className='stars'>
                        {_.range(1, 6).map((r) => (

                          <span key={r} onClick={(ev) => handleSelect(ev)}
                                className={(update?6-hotelUpdate.hotelStar:stars) > r ? 'hotelRegister-rating-stars' : 'hotelRegister-rating-select'}
                                id={r}>★</span>
                        ))}
                    </div>
                </div>
                <button onClick={ev => handleSubmit(ev)} className='hotelRegister-submit'>{update?"Update":'Submit'}</button>
            </form>}
        </div>
    );
}

export default RegisterHotel;



function RegisterRooms(props) {
    const dispatch=useDispatch()
    const [img,setImg]=useState([])
    const [status,setStatus]=useState(false)
    const [price,setPrice]=useState(0)
    const [roomCount,setRoomCount]=useState(null)
    const  handleFile=(e)=>{
        let files=e.target.files
           setImg([...img,...files])
    }
    const handleChangeCount=(e)=>{
        setRoomCount(e.target.value)
    }
    const handleSubmit=(e)=>{
        e.preventDefault()
        if (img.length===0||roomCount===null){
            error("Please select a picture")
            return
        }
        dispatch(getRoomsCreate(img,status,roomCount,price,props.hotelID))
        setImg([])

    }
    return (
      <>
          <form className="signup-form">
              <div className="form-header">
                  <div className="back">
                      <div   className="back-arrow">
                          <MdArrowBack/>
                      </div>
                  </div>
                  <div className='hotelRegister-title'>
                      <h1>Register rooms</h1>
                  </div>
              </div>
              <div className='hotelRegister-form-body'>
                  <label htmlFor="hotelName" className='hotelRegister-form-body-label'>Price </label>
                  <input id='hotelName' value={price}  required placeholder='Price' onChange={(e)=>{setPrice(e.target.value)}} name='hotelName' type="text"/>
              </div>
              <select style={{width:"35%",height:42,marginTop:65,marginLeft:30}} onChange={(ev)=>{handleChangeCount(ev)}}>
                  <option  value="1">1</option>
                  <option  value="2">2</option>
                  <option  value="3">3</option>
                  <option  value="famili">Famil</option>

              </select>
              <div className='hotelRegister-form-body'>
                  <input  id='hotelName' value={status} required placeholder='Status' name='hotelName' type="checkbox"/>
                  <label htmlFor="hotelName" className='hotelRegister-form-body-label'>Status </label>
              </div>
              <div className='hotelRegister-form-body'>
                  <label htmlFor="hotelAddress" className='hotelRegister-form-body-label'>Rooms Img </label>
                  <input
                    multiple
                    id='address'
                    onChange={(event) =>{ handleFile(event)}}
                    placeholder=''
                    name='file'
                    type="file"
                  />
              </div>
              <button onClick={(event)=>{handleSubmit(event)}}  className='hotelRegister-submit'>Submit</button>
          </form>
      </>
    );
}





