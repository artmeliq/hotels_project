import React from 'react';

import RegisterGoogle from "./RegisterGoogle";
import {useNavigate} from "react-router-dom";

function RegisterCard(props) {
  const navigate=useNavigate()
  const handleClose=()=>{
    navigate('/login')
  }
  return (
    <>
      <div className="card bg-light" style={{width:500, marginTop:-30}}>
        <div className='closeRegister' onClick={()=>{handleClose()}}>X</div>
        <article className=" card-body mx-auto" style={{maxWidth: 400}}>
          <h4 className="card-title mt-3 text-center">Create Account</h4>
          <p className="a_margin text-center">Get started with your free account</p>
          <p>
            <RegisterGoogle/>
            <a href="" className="btn btn-block btn-facebook"> <i
              className="fab fa-facebook-f"></i> Login via facebook</a>
          </p>
          <p className="divider-text">
            <span className="bg-light">OR</span>
          </p>
          {props.children}
        </article>
      </div>
    </>
  );
}

export default RegisterCard;