import React from 'react';

function RegisterPhone(props) {
  return (
    <div key={props.id} className="inputMargin form-group input-group">
      <div className="input-group-prepend">
        <span className="input-group-text"> <i className="fa fa-phone"></i> </span>
      </div>
      <select onChange={(e)=>{props.setPhone(e.target.value)}} className="custom-select" style={{maxWidth: 120}}>
        <option key={374} selected="">+374</option>
      </select>
      <input name="" className={props.registerUser[props.title].error?'form-control activeError':"form-control"} placeholder={props.title} type="number"
             onChange={(e) => {
               props.handleClick(e)
             }}/>
    </div>
  );
}

export default RegisterPhone;