import React from 'react';

function Footer(props) {
    return (
        <div className="footer-section">
            <div className="container">
                <div className="footer-top">
                    <p>&copy; 2015 Hospelry. All rights reserved | Design by <a
                        href="http://w3layouts.com">W3layouts</a></p>
                </div>

            </div>
        </div>
    );
}

export default Footer;