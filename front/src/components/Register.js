import React, {useEffect, useState} from 'react';
 import _ from 'lodash'
import REGISTER from '../data/register.json'
import {useDispatch, useSelector} from "react-redux";
import {getRegister} from "../store/action/register";
import {useNavigate} from "react-router-dom";
import RegisterCard from "./register/RegisterCard";
import RegisterPhone from "./register/RegisterPhone";
import RegisterGender from "./register/RegisterGender";
import RegisterInput from "./register/RegisterInput";
import {error} from "./svg/Popup";
const RegisterList=['Name',"Surname","Repeat_password","Create_password","email"]

function Register(props) {
    const dispatch = useDispatch()
    const [test,setTest]=useState(false)
    const [phone,setPhone]=useState('374')
    const navigate=useNavigate()
    let statusRegister=useSelector(store => store.register.status)
  //todo error-ner@ inputi borderi guynn en
    const [registerUser,setRegisterUser]=useState({
        email:{email: '',error:false},
        Gender:{Gender: '',error:false},
        Role:{Role: '',error:false},
        Surname:{Surname: '',error:false},
        Name:{Name: '',error:false},
        phone:{phone: '',error:false},
        Repeat_password:{Repeat_password: '',error:false},
        Create_password:{Create_password: '',error:false},
    })
    useEffect(()=>{
        if (statusRegister === 'Registration was successful'){
            navigate('/')
        }
    },[statusRegister])

    const handleSelect=(e)=>{
        if (e.target.value==='m'||e.target.value==='f'){
            registerUser.Gender.Gender=e.target.value
        }else if(e.target.value==='Admin'||e.target.value==='User'){

            registerUser.Role.Role=e.target.value
        }

    }
    const handleRegister = (e) => {
        let registerError=false
        e.preventDefault()
      let data= registerUser
        _.map(data, (index, key) => {

            if (index[key] === '') {
                index.error = true
                registerError=true
            } else {
                index.error = false
            }
        })
        setTest(!test)
        if (registerError===true){
            error("Fill in all the lines")
            return;
        }
        setRegisterUser(data)
        if (registerUser.Repeat_password.Repeat_password!==registerUser.Create_password.Create_password){
            registerUser.Repeat_password.error=true
            error("Repeated password is incorrect")
            return
        }
        dispatch(getRegister(registerUser))

    }
    const handleClick = (e) => {
        let name = e.target.placeholder.toString()
        if (name==='phone'){
            registerUser[name][name] =phone + e.target.value
            return;
        }
        registerUser[name][name] = e.target.value
    }

    return (
        <div className='registerContainer container'>
            <div className="container">
                    <hr/>
                        <RegisterCard>
                                <form>
                                    {_.map(
                                    REGISTER,(item=>{
                                       if (RegisterList.includes(item.title)){
                                           return( <RegisterInput  key={item.id} id={item.id} title={item.title}  handleClick={handleClick} registerUser={registerUser}/>)
                                        }
                                        if (item.title==="phone"){
                                           return (<RegisterPhone key={item.id}  id={item.id} title={item.title} setPhone={setPhone} handleClick={handleClick} registerUser={registerUser}/>)
                                        }
                                        if (item.title==="Gender"|| item.title==="Role"){
                                         return (<RegisterGender key={item.id}  id={item.id} title={item.title} handleSelect={handleSelect} registerUser={registerUser} option={item.option}/>)
                                        }
                                    })
                                )
                                }

                                    <div className="inputMargin form-group">
                                        <button onClick={(event)=>{handleRegister(event)}}  className="btn btn-primary btn-block"> Create Account
                                        </button>
                                    </div>
                                    <p className="text-center">Have an account? <a href="">Log In</a></p>
                                </form>

                        </RegisterCard>
            </div>
        </div>
    );
}

export default Register;